###############################################################################
cmake_minimum_required(VERSION 3.7.0)
project(cruft-crypto CXX)


list (APPEND components
    hash/blake
    hash/blake2
    hash/md2
    hash/md4
    hash/md5
    hash/md6
    hash/ripemd
    hash/sha1
    hash/sha2
    hash/sha3
    hash/tiger
    hash/pbkdf2
    hash/hmac
    hash/hotp
    stream/chacha
    stream/rabbit
    stream/rc4
    stream/salsa
    block/rc2
    block/rc5
    block/rc6
    block/speck
    block/tea
    block/xtea
    block/xxtea
)


###############################################################################
list (APPEND sources fwd.hpp)
foreach (c ${components})
    list (APPEND sources "${c}.cpp" "${c}.hpp")
endforeach()

list (APPEND sources
    hash/blake/traits.cpp
    hash/blake/traits.hpp
)


option (TESTS "enable unit testing" ON)

if (TESTS)
    include (CTest)
    enable_testing ()

    foreach (t ${components})
        string(REPLACE "/" "_" name "test/${t}")
        add_executable(crypto_${name} test/${t}.cpp)
        target_link_libraries(crypto_${name} PRIVATE cruft-crypto)
        target_include_directories(crypto_${name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
        add_test(NAME crypto_${name} COMMAND crypto_${name})
    endforeach()
endif ()


###############################################################################
add_library (cruft-crypto STATIC ${sources})
target_link_libraries (cruft-crypto INTERFACE cruft)


##-----------------------------------------------------------------------------
add_executable (crypto_bench tools/bench.cpp)
target_link_libraries (crypto_bench cruft-crypto)


##-----------------------------------------------------------------------------
add_executable (crypto_hash tools/hash.cpp)
target_link_libraries (crypto_hash cruft-crypto)


###############################################################################
configure_file(libcruft-crypto.pc.in libcruft-crypto.pc)
configure_file(Doxyfile.in Doxyfile)
