///////////////////////////////////////////////////////////////////////////////
#include "hash/blake.hpp"

#include <cruft/util/ascii.hpp>
#include <cruft/util/tap.hpp>

using cruft::crypto::hash::blake;


///////////////////////////////////////////////////////////////////////////////
static const struct {
    std::vector<uint8_t> data;
    std::vector<uint8_t> salt;
    blake<224>::digest_t d224;
    blake<256>::digest_t d256;
    blake<384>::digest_t d384;
    blake<512>::digest_t d512;
    const char *message;
} TESTS[] = {
    {
        {},
        {},
        "7dc5313b1c04512a174bd6503b89607a"
        "ecbee0903d40a8a569c94eed"_hex2array,
        "716f6e863f744b9ac22c97ec7b76ea5f"
        "5908bc5b2f67c61510bfc4751384ea7a"_hex2array,
        "c6cbd89c926ab525c242e6621f2f5fa7"
        "3aa4afe3d9e24aed727faaadd6af38b6"
        "20bdb623dd2b4788b1c8086984af8706"_hex2array,
        "a8cfbbd73726062df0c6864dda65defe"
        "58ef0cc52a5625090fa17601e1eecd1b"
        "628e94f396ae402a00acc9eab77b4d4c"
        "2e852aaaa25a636d80af3fc7913ef5b8"_hex2array,
        //"716f6e863f744b9ac22c97ec7b76ea5f"
        //"5908bc5b2f67c61510bfc4751384ea7a"_hex2array,
        "empty, default salt"
    },
    {
        { 0x00 },
        { },
        "4504cb0314fb2a4f7a692e696e487912"
        "fe3f2468fe312c73a5278ec5"_hex2array,
        "0ce8d4ef4dd7cd8d62dfded9d4edb0a7"
        "74ae6a41929a74da23109e8f11139c87"_hex2array,
        "10281f67e135e90ae8e882251a355510"
        "a719367ad70227b137343e1bc122015c"
        "29391e8545b5272d13a7c2879da3d807"_hex2array,
        "97961587f6d970faba6d2478045de6d1"
        "fabd09b61ae50932054d52bc29d31be4"
        "ff9102b9f69e2bbdb83be13d4b9c0609"
        "1e5fa0b48bd081b634058be0ec49beb3"_hex2array,
        //"0ce8d4ef4dd7cd8d62dfded9d4edb0a7"
        //"74ae6a41929a74da23109e8f11139c87"_hex2array,
        "NIST 8 bit zeros, default salt",
    },
    {
        "000102030405060708090a0b0c0d0e0f"_hex2u08,
        {},
        "41729acd98684538f559a789035bd893"
        "b644f38942a8b5614da5a8a6"_hex2array,
        "0f899aa730b1b1c5fa8ff2a5013a6f5f"
        "cbe5fdac46f18e2a9a9ecb9a99160219"_hex2array,
        "b718141c2ca282eb2b91e021444d5bab"
        "dc812d13af25d210c0e10423d10e6024"
        "5b113161ae0b7d80a60be1794c09a925"_hex2array,
        "47bc13549402bb583227ec8c4c408136"
        "ab4ecc5dbb11a98a2e755388c8ca11a7"
        "521d1d375cf3afe5d958013c9c505493"
        "2c3b0fc76a8ff7a1dead2c0b46bdd1f0"_hex2array,
        //"0f899aa730b1b1c5fa8ff2a5013a6f5f"
        //"cbe5fdac46f18e2a9a9ecb9a99160219"_hex2array,
        "16 bytes, default salt"
    },
    {

        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b"_hex2u08,
        {},
        "483d8043171efeb3a6d52e7387f2e90b"
        "c81ebeeb647412aa8837b063"_hex2array,
        "755f9ce025a066492e2b29c47462c985"
        "4ede6874f43467e7fb548172335b4d50"_hex2array,
        "28eed2bb102cd19344c9857b29c8adf3"
        "11eeaaed0334bc66809bbf0a515171a3"
        "cc5a13373a47f16f975290691afb6ec1"_hex2array,
        "0a47642c0b54f52cb780a2f425c6e81b"
        "4d3d26d60b3ba6e7c7d2f164e28041e2"
        "83c4366fd94644d2a6ba5dd7ddb33b70"
        "979bbdf6169436840d34a0f18ad7debc"_hex2array,
        //"755f9ce025a066492e2b29c47462c985"
        //"4ede6874f43467e7fb548172335b4d50"_hex2array,
        "exactly pre-padding size, default salt"
    },
    {
        std::vector<uint8_t> (576/8, 0),
        {},
        "f5aa00dd1cb847e3140372af7b5c46b4"
        "888d82c8c0a917913cfb5d04"_hex2array,
        "d419bad32d504fb7d44d460c42c5593f"
        "e544fa4c135dec31e21bd9abdcc22d41"_hex2array,
        "e37d96e5ca54058c348e8c67cac1f93a"
        "a13fc64aa4bca19015a582e6505dcbc7"
        "d39d661b464d485fb74c75485b30c6c2"_hex2array,
        "2f31e729e269c7f22cac8b71cb80a504"
        "afe38350498e0d178afb38cc356dfdaf"
        "55d1e7d723c8ff686df0710c43d9abef"
        "9f0f61b91ab3b49f72b96f899159a735"_hex2array,
        //"d419bad32d504fb7d44d460c42c5593f"
        //"e544fa4c135dec31e21bd9abdcc22d41"_hex2array,
        "NIST 576 bit zeros, default salt",
    },
    {
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e"_hex2u08,
        { },
        "0a5e8da4f27db9afe24c29a3c6f92ec8"
        "831113f144c59a64c37a2016"_hex2array,
        "cfce445066d35322557b432540bd2f0a"
        "f4caf9f426568236d9944426a5df792a"_hex2array,
        "4688779d44b6d0ee1be74fce5a3bf8c0"
        "2069c816f01e68e541bd5e6772d6dade"
        "b79b6c3193a1c2732206885f29391030"_hex2array,
        "d8e947b518ad5700e94a32ebb2f4e5e7"
        "34a907637dd24f26ba675cd5f984bdda"
        "ce7392993576ff2ab387c19b3fbb920e"
        "7f0c1368f910756306070aea6affe1e7"_hex2array,
        //"cfce445066d35322557b432540bd2f0a"
        //"f4caf9f426568236d9944426a5df792a"_hex2array,
        "almost block size, default salt"
    },
    {
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"_hex2u08,
        { },
        "08b6960d0b0d6f864c9c2a2331341dd0"
        "1745cfbfad200888cdb4b5b7"_hex2array,
        "4432b2c1e983b0c326583516920f3949"
        "c2acf5d85a99353601228cab40c867bc"_hex2array,
        "917d92609d640f06cba773be8604d177"
        "492649c39a4ce85df643c0b72293053f"
        "f39967691cbb1809737eaf6734e1b185"_hex2array,
        "4d47291b807750d2ce6ced17ae71dc24"
        "f5a3205f4fe309537488242c4420cd32"
        "d997beda4d560200cbcf3e9d68143e69"
        "f08c54b82ce77db7c22d0e17b5a1363e"_hex2array,
        //"4432b2c1e983b0c326583516920f3949c2acf5d85a99353601228cab40c867bc"_hex2array,
        "exactly block size, default salt",
    },
};

//-----------------------------------------------------------------------------
// million 'a':
// 224: 738443f8093ae703ebe4fe991b4f00208701e2e7be1275fd1bd84ef1
// 256: 22be6de4aa4214c9403f10598f0a6b0e834570251a13bc27589437f7139a5d44
// 384: 22ccce05b0ac1ceda1b0b0dc0021e6a3957779260cd2fae8a21c3d5432ce204f1df2d62a00d8f505cae6e70e3be18701
// 512: e5d3ecf60cbf8ed8b44307ae6606f4c8eae0a602c2a393f529cf405a1384c791c3ce58f54838a76b89a6455e988d63f98222ea82bf26e11e970516fb7c62b41d
int
main ()
{
    cruft::TAP::logger tap;

    for (const auto &t: TESTS) {
        blake<256> h256;
        tap.expect_eq (h256 (t.data, t.salt), t.d256, "%s", t.message);
    }

    {
        blake<256> h256;
        std::vector<uint8_t> data (1'000'000, 'a');
        tap.expect_eq (
            h256 (data),
            "22be6de4aa4214c9403f10598f0a6b0e834570251a13bc27589437f7139a5d44"_hex2array,
            "million 'a', 256"
        );
    }

    return tap.status ();
}