#include "hash/tiger.hpp"

#include <cruft/util/tap.hpp>
#include <cruft/util/endian.hpp>
#include <cruft/util/ascii.hpp>


///////////////////////////////////////////////////////////////////////////////
static struct {
    char const *data;
    cruft::crypto::hash::tiger::digest_t digest;
    char const *description;
} const TESTS[] = {
    {
        .data = "",
        .digest = {
            cruft::btoh (0x3293ac630c13f024u),
            cruft::btoh (0x5f92bbb1766e1616u),
            cruft::btoh (0x7a4e58492dde73f3u)
        },
        .description = "<null>"
    },

    {
        .data = "a",
        .digest = {
            cruft::btoh (0x77befbef2e7ef8abu),
            cruft::btoh (0x2ec8f93bf587a7fcu),
            cruft::btoh (0x613e247f5f247809u),
        },
        .description = "a",
    },

    {
        .data = "abc",
        .digest = {
            cruft::btoh (0x2aab1484e8c158f2u),
            cruft::btoh (0xbfb8c5ff41b57a52u),
            cruft::btoh (0x5129131c957b5f93u),
        },
        .description = "abc"
    },

    {
        .data = "message digest",
        .digest = {
            cruft::btoh (0xd981f8cb78201a95u),
            cruft::btoh (0x0dcf3048751e441cu),
            cruft::btoh (0x517fca1aa55a29f6u),
        },
        .description = "message digest",
    },

    {
        .data = "abcdefghijklmnopqrstuvwxyz",
        .digest = {
            cruft::btoh (0x1714a472eee57d30u),
            cruft::btoh (0x040412bfcc55032au),
            cruft::btoh (0x0b11602ff37beee9u),
        },
        .description = "abcdefghijklmnopqrstuvwxyz",
    },

    {
        .data = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
        .digest = {
            cruft::btoh (0x0f7bf9a19b9c58f2u),
            cruft::btoh (0xb7610df7e84f0ac3u),
            cruft::btoh (0xa71c631e7b53f78eu),
        },
        .description = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
    },

    {
        .data = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
        .digest = {
            cruft::btoh (0x8dcea680a17583eeu),
            cruft::btoh (0x502ba38a3c368651u),
            cruft::btoh (0x890ffbccdc49a8ccu),
        },
        .description = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    },

    {
        .data = "12345678901234567890123456789012345678901234567890123456789012345678901234567890",
        .digest = {
            cruft::btoh (0x1c14795529fd9f20u),
            cruft::btoh (0x7a958f84c52f11e8u),
            cruft::btoh (0x87fa0cabdfd91bfdu),
        },
        .description = "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
    },
};


//-----------------------------------------------------------------------------
static struct {
    std::size_t count;
    cruft::crypto::hash::tiger::digest_t digest;
} const REPEATS[] {
    { .count = 1024u, .digest = {
        cruft::btoh (0xcdf0990c5c6b6b0bu),
        cruft::btoh (0xddd63a75ed20e2d4u),
        cruft::btoh (0x48bf44e15fde0df4u)
    } },
    { .count = 1025u, .digest = {
        cruft::btoh (0x89292aee0f82842au),
        cruft::btoh (0xbc080c57b3aadd9cu),
        cruft::btoh (0xa84d66bf0cae77aau)
    } },
};


///////////////////////////////////////////////////////////////////////////////
int main ()
{
    cruft::TAP::logger tap;

    for (auto const &t: TESTS) {
        auto const res = cruft::crypto::hash::tiger {} (cruft::view (t.data).cast<u08 const*> ());
        tap.expect_eq (t.digest, res, "%s", t.description);
    }

    for (auto const &t: REPEATS) {
        std::vector<char> data (t.count);
        std::fill_n (data.begin (), t.count, 'A');

        auto const res = cruft::crypto::hash::tiger {} (cruft::view (data).cast<u08 const*> ());
        tap.expect_eq (t.digest, res, "%! 'A's", t.count);
    };

    return tap.status ();
}