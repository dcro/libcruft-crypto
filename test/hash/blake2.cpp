#include <cruft/util/ascii.hpp>
#include <cruft/util/tap.hpp>

#include "hash/blake2.hpp"

using cruft::crypto::hash::blake2;


///////////////////////////////////////////////////////////////////////////////
static const struct {
    std::vector<uint8_t> data;
    std::vector<uint8_t> salt;
    blake2::digest_t  digest;
    const char *message;
} TESTS2b[] = {
    {
        {},
        {},
        "786a02f742015903c6c6fd852552d272"
        "912f4740e15847618a86e217f71f5419"
        "d25e1031afee585313896444934eb04b"
        "903a685b1448b755d56f701afe9be2ce"_hex2array,
        "empty"
    },
    {
        {},
        "00"_hex2u08,
        "aaf42280524929171e417e77be67f9ed"
        "ec3a8461bbe7b5c2bd1d9a3d0928f1db"
        "bd1f6600bb866b72f0e3b3e22282c145"
        "f69873a3d250ddc43c423685d1247657"_hex2array,
        "empty, 0x00 salt",
    },
    {
        {},
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e"_hex2u08,
        "e350b6f7642eea11bb1bd9c6393f9e89"
        "a0d10f9e38a754ca5f61838117ba0dd9"
        "6af3f3dbc092acc89e8a8390aee233f0"
        "d2b0adf32f009073352264ba5d317f2d"_hex2array,
        "empty, 63 byte salt",
    },
    {
        {},
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"_hex2u08,
        "10ebb67700b1868efb4417987acf4690"
        "ae9d972fb7a590c2f02871799aaa4786"
        "b5e996e8f0f4eb981fc214b005f42d2f"
        "f4233499391653df7aefcbc13fc51568"_hex2array,
        "empty, 64 byte salt",
    },
    {
        "00"_hex2u08,
        {},
        "2fa3f686df876995167e7c2e5d74c4c7"
        "b6e48f8068fe0e44208344d480f7904c"
        "36963e44115fe3eb2a3ac8694c28bcb4"
        "f5a0f3276f2e79487d8219057a506e4b"_hex2array,
        "0x00, no salt"
    },
    {
        "000102"_hex2u08,
        {},
        "40a374727302d9a4769c17b5f409ff32"
        "f58aa24ff122d7603e4fda1509e919d4"
        "107a52c57570a6d94e50967aea573b11"
        "f86f473f537565c66f7039830a85d186"_hex2array,
        "0x000102, no salt"
    },
    {
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e"_hex2u08,
        {},
        "d10bf9a15b1c9fc8d41f89bb140bf0be"
        "08d2f3666176d13baac4d381358ad074"
        "c9d4748c300520eb026daeaea7c5b158"
        "892fde4e8ec17dc998dcd507df26eb63"_hex2array,
        "almost block size, no salt",
    },
    {
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"_hex2u08,
        {},
        "2fc6e69fa26a89a5ed269092cb9b2a44"
        "9a4409a7a44011eecad13d7c4b045660"
        "2d402fa5844f1a7a758136ce3d5d8d0e"
        "8b86921ffff4f692dd95bdc8e5ff0052"_hex2array,
        "exactly block size, no salt"
    },
    {
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"_hex2u08,
        "000102030405060708090a0b0c0d0e0f"
        "101112131415161718191a1b1c1d1e1f"
        "202122232425262728292a2b2c2d2e2f"
        "303132333435363738393a3b3c3d3e3f"_hex2u08,
        "65676d800617972fbd87e4b9514e1c67"
        "402b7a331096d3bfac22f1abb95374ab"
        "c942f16e9ab0ead33b87c91968a6e509"
        "e119ff07787b3ef483e1dcdccf6e3022"_hex2array,
        "exactly block size, 64 byte salt"
    },
};


//-----------------------------------------------------------------------------
int
main ()
{
    cruft::TAP::logger tap;

    for (const auto &t: TESTS2b) {
        blake2 h (t.salt);
        auto digest = h (t.data);
        tap.expect_eq (digest, t.digest, "%s", t.message);
    }

    {
        blake2 h;
        const std::vector<uint8_t> million (1'000'000, 'a');
        tap.expect_eq (
            h (million),
            "98fb3efb7206fd19ebf69b6f312cf7b6"
            "4e3b94dbe1a17107913975a793f177e1"
            "d077609d7fba363cbba00d05f7aa4e4f"
            "a8715d6428104c0a75643b0ff3fd3eaf"_hex2array,
            "million 'a'"
        );
    }

    return tap.status ();
}