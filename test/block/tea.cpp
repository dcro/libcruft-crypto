#include "block/tea.hpp"

#include <cruft/util/tap.hpp>
#include <cruft/util/types.hpp>

using cruft::crypto::block::TEA;


int
main ()
{
    // test vectors from 'TeaCrypt', by Logan J. Drews.
    static const struct {
        std::array<uint32_t,4> key;
        std::array<uint32_t,2> dec;
        std::array<uint32_t,2> enc;
    } TESTS[] = {

        {
            { 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
            { 0x00000000, 0x00000000 },
            { 0x41EA3A0A, 0x94BAA940 },
        },

        {
            { 0x00000000, 0x00000000, 0x00000000, 0x00000000 },
            { 0x01020304, 0x05060708 },
            { 0x6A2F9CF3, 0xFCCF3C55 },
        },

        {
            { 0x00112233, 0x44556677, 0x8899AABB, 0xCCDDEEFF },
            { 0x01020304, 0x05060708 },
            { 0xDEB1C0A2, 0x7E745DB3 },
        },

        {
            { 0x00112233, 0x44556677, 0x8899AABB, 0xCCDDEEFF },
            { 0x01234567, 0x89ABCDEF },
            { 0x126C6B92, 0xC0653A3E },
        },
    };

    cruft::TAP::logger tap;

    for (size_t i = 0; i < std::size (TESTS); ++i) {
        const auto &t = TESTS[i];
        TEA gen (t.key);

        std::array<uint32_t,2> enc;
        gen.encrypt (cruft::view (enc), cruft::view (t.dec));

        std::array<uint32_t,2> dec;
        gen.decrypt (cruft::view (dec), cruft::view (t.enc));
        
        tap.expect (enc == t.enc, "TEA_enc %zu", i);
        tap.expect (dec == t.dec, "TEA_dec %zu", i);
    }

    return tap.status ();
}
