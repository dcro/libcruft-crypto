#include <cruft/util/tap.hpp>

#include "block/speck.hpp"


int
main (void)
{
    cruft::TAP::logger tap;

    {
        // speck128/128 test vectors from the IACR paper.
        std::array<u64,2> const key { 0x0f0e0d0c0b0a0908, 0x0706050403020100 };
        std::array<u64,2> const dec { 0x6c61766975716520, 0x7469206564616d20 };
        std::array<u64,2> const enc { 0xa65d985179783265, 0x7860fedf5c570d18 };

        cruft::crypto::block::speck const cipher (key);
        tap.expect (cipher.encrypt (dec) == enc, "speck128/128 encode");
        tap.expect (cipher.decrypt (enc) == dec, "speck128/128 decode");
    }

    return tap.status ();
}