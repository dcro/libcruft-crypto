#include "block/xtea.hpp"

#include <cruft/util/tap.hpp>
#include <cruft/util/types.hpp>


int
main ()
{
    // test vectors from 'TeaCrypt', by Logan J. Drews.
    static const struct {
        std::array<uint32_t,4> key;
        std::array<uint32_t,2> dec;
        std::array<uint32_t,2> enc;
    } TESTS[] = {
        {
            {0x00000000, 0x00000000, 0x00000000, 0x00000000},
            {0x00000000, 0x00000000},
            {0xDEE9D4D8, 0xF7131ED9},
        },

        {
            {0x00000000, 0x00000000, 0x00000000, 0x00000000},
            {0x01020304, 0x05060708},
            {0x065C1B89, 0x75C6A816},
        },

        {
            {0x00112233, 0x44556677, 0x8899AABB, 0xCCDDEEFF},
            {0x01020304, 0x05060708},
            {0xDCDD7ACD, 0xC1584B79},

        },

        {
            {0x00112233, 0x44556677, 0x8899AABB, 0xCCDDEEFF},
            {0x01234567, 0x89ABCDEF},
            {0xB8BF2821, 0x622B5B30},
        },
    };

    cruft::TAP::logger tap;

    for (size_t i = 0; i < std::size (TESTS); ++i) {
        const auto &t = TESTS[i];
        cruft::crypto::block::XTEA gen (t.key);

        std::array<uint32_t,2> enc_result;
        gen.encrypt (cruft::view (enc_result), cruft::view (t.dec));

        std::array<uint32_t,2> dec_result;
        gen.decrypt (cruft::view (dec_result), cruft::view (enc_result));

        tap.expect (enc_result == t.enc, "XTEA_enc %zu", i);
        tap.expect (dec_result == t.dec, "XTEA_dec %zu", i);
    }

    return tap.status ();
}
