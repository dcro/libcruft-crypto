#include <cruft/util/tap.hpp>

#include "block/rc2.hpp"

#include <cruft/util/endian.hpp>


int main ()
{
    struct {
        unsigned effective;
        std::vector<u08> key;
        std::array<u16,4> plain;
        std::array<u16,4> cipher;
    } const TESTS[] = {
        {
            .effective = 63,
            .key = { 0x00, 0x00, 0x00, 0x00,
                     0x00, 0x00, 0x00, 0x00, },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000, },
            .cipher = { 0xebb7, 0x73f9, 0x9327, 0x8eff, }
        },

        {
            .effective = 64,
            .key = { 0xff, 0xff, 0xff, 0xff,
                     0xff, 0xff, 0xff, 0xff, },
            .plain  = { 0xffff, 0xffff, 0xffff, 0xffff },
            .cipher = { 0x278b, 0x27e4, 0x2e2f, 0x0d49 },
        },

        {
            .effective = 64,
            .key = { 0x30, 0x00, 0x00, 0x00,
                     0x00, 0x00, 0x00, 0x00 },
            .plain  = { 0x1000, 0x0000, 0x0000, 0x0001 },
            .cipher = { 0x3064, 0x9edf, 0x9be7, 0xd2c2 },
        },

        {
            .effective = 64,
            .key    = { 0x88 },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000 },
            .cipher = { 0x61a8, 0xa244, 0xadac, 0xccf0 },
        },

        {
            .effective = 64,
            .key = { 0x88, 0xbc, 0xa9, 0x0e,
                     0x90, 0x87, 0x5a },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000, },
            .cipher = { 0x6ccf, 0x4308, 0x974c, 0x267f, }
        },

        {
            .effective = 64,
            .key = { 0x88, 0xbc, 0xa9, 0x0e,
                     0x90, 0x87, 0x5a, 0x7f,
                     0x0f, 0x79, 0xc3, 0x84,
                     0x62, 0x7b, 0xaf, 0xb2
            },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000 },
            .cipher = { 0x1a80, 0x7d27, 0x2bbe, 0x5db1 },
        },

        {
            .effective = 128,
            .key = {
                0x88, 0xbc, 0xa9, 0x0e,
                0x90, 0x87, 0x5a, 0x7f,
                0x0f, 0x79, 0xc3, 0x84,
                0x62, 0x7b, 0xaf, 0xb2,
            },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000 },
            .cipher = { 0x2269, 0x552a, 0xb0f8, 0x5ca6 },
        },

        {
            .effective = 129,
            .key = {
                0x88, 0xbc, 0xa9, 0x0e,
                0x90, 0x87, 0x5a, 0x7f,
                0x0f, 0x79, 0xc3, 0x84,
                0x62, 0x7b, 0xaf, 0xb2,
                0x16, 0xf8, 0x0a, 0x6f,
                0x85, 0x92, 0x05, 0x84,
                0xc4, 0x2f, 0xce, 0xb0,
                0xbe, 0x25, 0x5d, 0xaf,
                0x1e
            },
            .plain  = { 0x0000, 0x0000, 0x0000, 0x0000 },
            .cipher = { 0x5b78, 0xd3a4, 0x3dff, 0xf1f1 },
        }
    };


    cruft::TAP::logger tap;

    for (auto const &t: TESTS) {
        cruft::crypto::block::rc2 const obj (t.key, t.effective);

        std::array<u16,4> plain = t.plain;
        std::transform (
            std::begin (plain),
            std::end   (plain),
            std::begin (plain),
            static_cast<u16(&)(u16)> (cruft::bswap)
        );

        std::array<u16,4> cipher = t.cipher;
        std::transform (
            std::begin (cipher),
            std::end   (cipher),
            std::begin (cipher),
            static_cast<u16(&)(u16)> (cruft::bswap)
        );

        tap.expect (obj.encrypt (plain) == cipher, "rc2_enc");
        tap.expect (obj.decrypt (cipher) == plain,  "rc2_dec");
    }

    return tap.status ();
}
