
#if 0
#include <cstdint>

uint32_t R(a,b,c,k)
{
    b += c;
    a ^= b;
    return rotatel (a, k);
}


void quarter (a, b, c, d)
{
    a += b; d ^= a; rotatel (d, 16);
    c += d; b ^= c; rotatel (b, 12);
    a += b; d ^= a; rotatel (d,  8);
    c += d; b ^= c; rotatel (b,  7);
}


void double_round ()
{
    QR (0, 4, 8, 12)
    QR (1, 5, 9, 13)
    QR (2, 6, 10, 14)
    QR (3, 7, 11, 15)
    QR (0, 5, 10, 15)
    QR (1, 6, 11, 12)
    QR (2, 7, 8, 13)
    QR (3, 4, 9, 14)
}
#endif
