#if 0

#include <cstdint>

// rabbit, rfc4503

struct state {
    state () {
        // key setup
        for (int j = 0; j < 8; ++j) {
            if (j % 2 == 0) {
                X[j] = K[(j + 1) % 8] << 16 | K[j];
                C[j] = K[(j + 4) % 8] << 16 | K[(j + 5) % 8];
            } else {
                X[j] = K[(j + 5) % 8] << 16 | K[(j+4) % 8];
                C[j] = K[j] << 16 | K[(j+1) % 8];
            }
        }

        for (int i = 0; i < 4; ++i) {
            update_counter ();
            next_state ();
        }

        for (int j = 0; j < 8; ++j)
            C[j] = C[j] ^ X[(j+4) % 8];

        // iv setup, optional? uint16_t IV[4];
        // 15...0: 0
        // 31..16: 1
        // 47..32: 2
        // 63..48: 3
        C[0] = C[0] ^ cat(IV[1], IV[0]);
        C[1] = C[1] ^ cat(IV[3], IV[1]);
        C[2] = C[2] ^ cat(IV[3], IV[2]);
        C[3] = C[3] ^ cat(IV[2], IV[0]);
        C[4] = C[4] ^ cat(IV[1], IV[0]);
        C[5] = C[5] ^ cat(IV[3], IV[1]);
        C[6] = C[6] ^ cat(IV[3], IV[2]);
        C[7] = C[7] ^ cat(IV[2], IV[0]);
    }

    void update_counter (void) {
        A[0] = 0x4D34D34D;
        A[1] = 0xD34D34D3;
        A[2] = 0x34D34D34;
        A[3] = 0x4D34D34D;
        A[4] = 0xD34D34D3;
        A[5] = 0x34D34D34;
        A[6] = 0x4D34D34D;
        A[7] = 0xD34D34D3;

        for (int j = 0; j < 8; ++j) {
            uint32_t temp = C[j] + A[j] + b;
            b = temp / WORDSIZE;
            C[j] = temp % WORDSIZE;
        }
    }

    auto square (auto v) { return (v % WORDSIZE) * (v % WORDSIZE); }
    auto g (auto u, auto v) {
        return LSW (square (u+v) ^ MSW (square (u+v)));
    }

    void next_state (void) {
        for (int j = 0; j < 8; ++j)
            G[j] = g(X[j], C[j]);

        X[0] = G[0] + rotatel (G[7], 16) + rotatel (G[6], 16) % WORDSIZE;
        X[1] = G[1] + rotatel (G[0],  8) +          G[7]      % WORDSIZE;
        X[2] = G[2] + rotatel (G[1], 16) + rotatel (G[0], 16) % WORDSIZE;
        X[3] = G[3] + rotatel (G[2],  8) +          G[1]      % WORDSIZE;
        X[4] = G[4] + rotatel (G[3], 16) + rotatel (G[2], 16) % WORDSIZE;
        X[5] = G[5] + rotatel (G[4],  8) +          G[3]      % WORDSIZE;
        X[6] = G[6] + rotatel (G[5], 16) + rotatel (G[4], 16) % WORDSIZE;
        X[7] = G[7] + rotatel (G[6],  8) +          G[5]      % WORDSIZE;
    }

    void extract (uint16_t S[8])
    {
        S[0] = LO(X[0]) ^ HI(X[5]);
        S[1] = HI(X[0]) ^ LO(X[3]);
        S[2] = LO(X[2]) ^ HI(X[7]);
        S[3] = HI(X[2]) ^ LO(X[5]);
        S[4] = LO(X[4]) ^ HI(X[1]);
        S[5] = HI(X[4]) ^ LO(X[7]);
        S[6] = LO(X[6]) ^ HI(X[3]);
        S[7] = HI(X[6]) ^ LO(X[1]);
    }




    uint32_t X[8]; // state
    uint32_t C[8]; // counter
    uint32_t b = 0; // carry bit
};

struct key {
    uint16_t k[8];
};
#endif
