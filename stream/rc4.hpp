/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_CRYPTO_STREAM_RC4_HPP
#define CRUFT_CRYPTO_STREAM_RC4_HPP

#include <array>
#include <cstdint>
#include <cstddef>

namespace cruft::crypto::stream {
    class RC4 {
    public:
        RC4 (const uint8_t *restrict key, size_t len);

        void discard (size_t);
        void generate (uint8_t *restrict dst, size_t len);

    private:
        uint8_t get (void);

        size_t x, y;
        std::array<uint8_t,256> S;
    };
}

#endif
