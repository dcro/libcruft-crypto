/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017-2018 Danny Robson <danny@nerdcruft.net>
 */


#include "./salsa.hpp"


///////////////////////////////////////////////////////////////////////////////
std::array<uint8_t,64>
cruft::crypto::stream::salsa20 (const std::array<uint8_t,64> bytes) noexcept
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
    auto x = *reinterpret_cast<const std::array<uint32_t,16>*> (&bytes);
    auto y = x;

    for (auto &t: x)
        t = cruft::ltoh (t);

    for (int i = 0; i < 10; ++i)
        x = salsa::doubleround (x);

    for (size_t i = 0; i < std::size (y); ++i)
        x[i] += y[i];

    for (auto &t: x)
        t = cruft::htol (t);

    return *reinterpret_cast<std::array<uint8_t,64>*> (&x);
#pragma GCC diagnostic pop
}
