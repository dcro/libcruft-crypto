#include "cruft/crypto/hash/blake.hpp"
#include "cruft/crypto/hash/blake2.hpp"
#include "cruft/crypto/hash/md2.hpp"
#include "cruft/crypto/hash/md4.hpp"
#include "cruft/crypto/hash/md5.hpp"
#include "cruft/crypto/hash/md6.hpp"
#include "cruft/crypto/hash/ripemd.hpp"
#include "cruft/crypto/hash/sha1.hpp"
#include "cruft/crypto/hash/sha2.hpp"
#include "cruft/crypto/hash/tiger.hpp"

#include <cruft/util/std.hpp>
#include <cruft/util/random.hpp>
#include <cruft/util/rand/pcg.hpp>
#include <cruft/util/preprocessor.hpp>
#include <cruft/util/debug/compiler.hpp>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
#include <vector>


///////////////////////////////////////////////////////////////////////////////
template <typename HashT>
float
time_hash (cruft::view<u08 const*> data)
{
    auto const start = std::chrono::high_resolution_clock::now ();
    HashT obj;
    auto const res = obj (data);
    cruft::debug::escape (res);
    auto const finish = std::chrono::high_resolution_clock::now ();

    std::chrono::duration<float> const duration = finish - start;
    return duration.count ();
}


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    // 1 GiB of random data;
    static constexpr std::size_t SOURCE_SIZE = 1024u * 1024u * 1024;
    std::vector<u08> source (SOURCE_SIZE);

    std::cerr << "generating data\n";
#if 1
    std::fill (std::begin (source), std::end (source), 0u);
#else
    std::generate_n (
        std::begin (source),
        SOURCE_SIZE,
        [] () { return cruft::random::uniform<u08> (); }
    );
#endif

    f32 const gib_multiplier = f32 (SOURCE_SIZE) / (1024 * 1024 * 1024);

    std::cerr << "starting benchmark\n";
    #define BENCH_HASH(KLASS) \
    do { \
        auto const elapsed = time_hash<cruft::crypto::hash::KLASS> (cruft::view (source)); \
        std::cout << #KLASS ": " << gib_multiplier / elapsed << " GiB/s\n"; \
    } while (0);

    MAP0 (BENCH_HASH,
        //MD2,
        MD4,
        MD5,
        RIPEMD,
        SHA1,
        SHA256,
        blake<512>,
        blake2,
        tiger
    )
}
