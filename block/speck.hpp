/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstddef>
#include <array>

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

namespace cruft::crypto::block {
    //template <
    //    std::size_t KeyBitsV,
    //    std::size_t BlockBitsV
    //>
    /// An implementation of the Speck block cipher.
    /// doi: 10.1145/2744769.2747946
    ///
    /// TODO: The block and key sizes are currently hard coded to 128 and 128
    ///       for ease of initial implementation. They should be parameters.
    class speck {
    public:
        using key_t = std::array<u64,2>;
        using word_t = u64;
        using block_t = std::array<word_t,2>;

        explicit speck (key_t);

        block_t encrypt (block_t src) const;
        block_t decrypt (block_t src) const;

    private:
        std::array<u64,32> m_key;
    };
}
