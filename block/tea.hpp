/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <array>
#include <cstdint>
#include <cstddef>

#include <cruft/util/view.hpp>
#include <cruft/util/std.hpp>


namespace cruft::crypto::block {
    /// An implementation of the "Tiny Encryption Algorithm" block cipher.
    ///
    /// ref: http://en.wikipedia.org/wiki/Tiny_Encryption_Algorithm
    class TEA {
    public:
        using key_t = std::array<u32,4>;
        using word_t = u32;
        static constexpr std::size_t block_size = sizeof (word_t) * 2;

        explicit TEA (key_t);

        /// Encrypt the contents of `src` into the buffer `dst`.
        ///
        /// src and dst are assumed to be equal sizes, multiples of block_size
        /// bytes, and suitably aligned for SIMD.
        ///
        /// \param dst   The location of the output
        /// \param src   The location of the input
        void encrypt (cruft::view<word_t*> dst, cruft::view<word_t const*> src);


        /// Decrypt the contents of `src` into the buffer `dst`.
        ///
        /// src and dst are assumed to be equal sizes, multiples of block_size
        /// bytes, and suitably aligned for SIMD.
        ///
        /// \param dst   The location of the output
        /// \param src   The location of the input
        void decrypt (cruft::view<word_t*> dst, cruft::view<word_t const*> src);

    private:
        key_t const m_key;
    };
}
