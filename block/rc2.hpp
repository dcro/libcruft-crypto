/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstddef>
#include <array>

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

namespace cruft::crypto::block {
    /// An implementation of the RC2 block cipher (RFC2268)
    class rc2 {
    public:
        using word_t = u16;
        using block_t = std::array<word_t,4>;

        explicit rc2 (cruft::view<u08 const*> key, int effective_bits);
        explicit rc2 (cruft::view<u08 const*> key);

        block_t encrypt (block_t src) const noexcept;
        block_t decrypt (block_t src) const noexcept;

    private:
        std::array<u16,64> m_key;
    };
}
