/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "xxtea.hpp"

#include <cstring>

// test vectors: http://www.cix.co.uk/~klockstone/teavect.htm

using cruft::crypto::block::XXTEA;


///////////////////////////////////////////////////////////////////////////////
static const u32 MAGIC = 0x9E3779B9;


//-----------------------------------------------------------------------------
static constexpr
u32
mix (u32 Z,
     u32 Y,
     u32 S,
     std::size_t E,
     std::size_t P,
     const u32 *restrict K)
{
    return ((Z >> 5 ^ Y << 2) + (Y >> 3 ^ Z << 4)) ^ ((S ^ Y) + (K[(P & 3) ^ E] ^ Z));
}


///////////////////////////////////////////////////////////////////////////////
XXTEA::XXTEA (key_t _key):
    m_key (_key)
{ ; }


///////////////////////////////////////////////////////////////////////////////
void
XXTEA::encrypt (cruft::view<word_t*> dst, cruft::view<word_t const*> src)
{
    if (dst.size () != src.size ())
        throw std::invalid_argument ("mismatching encode/decode buffer sizes");
    if (src.size () < 2)
        throw std::invalid_argument ("minimum blocksize is 64 bits");

    std::copy (src.begin (), src.end (), dst.begin ());

    auto const count = src.size ();
    uint32_t sum = 0;
    uint32_t z = dst[count - 1];
    uint32_t y, p;

    unsigned rounds = 6 + 52 / count;

    do {
        sum += MAGIC;
        uint32_t e = (sum >> 2) & 3;

        for (p = 0; p < count - 1; p++) {
            y = dst[p + 1];
            z = dst[p    ] += ::mix (z, y, sum, e, p, m_key.data ());
        }

        y = dst[0];
        z = dst[count - 1] += ::mix (z, y, sum, e, p, m_key.data ());
    } while (--rounds);
}


//-----------------------------------------------------------------------------
void
XXTEA::decrypt (cruft::view<word_t*> dst, cruft::view<word_t const*> src)
{
    if (dst.size () != src.size ())
        throw std::invalid_argument ("mismatching encode/decode buffer sizes");
    if (src.size () < 2)
        throw std::invalid_argument ("minimum blocksize is 64 bits");

    std::copy (src.begin (), src.end (), dst.begin ());

    auto const count = src.size ();
    uint32_t y, z, sum;
    uint32_t rounds;
    size_t p;

    rounds = 6 + 52 / count;
    sum = rounds * MAGIC;
    y = dst[0];

    do {
        uint32_t e = (sum >> 2) & 3;

        for (p = count - 1; p > 0; p--) {
            z = dst[p - 1];
            y = dst[p    ] -= ::mix (z, y, sum, e, p, m_key.data ());
        }

        z = dst[count - 1];
        y = dst[        0] -= ::mix (z, y, sum, e, p, m_key.data ());

        sum -= MAGIC;
    } while (--rounds);
}
