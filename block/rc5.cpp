#if 0
#include <cstdint>

// rfc2040


// w: word_size (in bits)
// u: word_size (in bytes)
// b: key_length (in bytes)
// K[]: key
// c: key_length (in words), or 1 if b==0
// L[]: schedule_temp
// r: rounds
// t: 2(r+1) round_subkeys
// S[]: subkey_words
// P_w: 1st magic, Odd((e-2)*2**w)
//   w=16 -> 0xb7e1
//   w=32 -> 0xb7e15163
//   w=64 -> 0xB7E151628AED2A6B
// Q_w: 2nd magic, Odd((phi-1)*2**w)
//   w=64 -> 0x9E3779B97F4A7C15

std::array<word_t,t>
generate_subkeys ()
{
    std::array<word_t,t> S;

    auto c = ceil (max (b, 1) / u);
    for (int i = b - 1; i >= 0; --i)
        L[i/u] = (L[i/u] << u) + K[i];

    S[0] = P_w;
    for (int i = 1; i < S.size (); ++i)
        S[i] = S[i-1] + Q_w;

    int i = 0;
    int j = 0;
    int A = 0;
    int B = 0;

    for (int times = 3*max (t, c); times; --times) {
        A = S[i] = (S[i] + A + B) <<< 3;
        B = L[j] = (L[j] + A + B) <<< (A + B);

        i = (i + 1) % t;
        j = (j + 1) % t;
    }

    return S;
}



word_t[2]
encrypt (word_t A, word_t B)
{
A += S[0];
B += S[1];

for (int i = 1; i <= r; ++i) {
A = ((A ^ B) <<< B) + S[2 * i];
B = ((B ^ A) <<< A) + S[2 * i + 1];
}

return {A,B};
}



word_t[2]
decrypt (word_t A, word_t B)
{
for (int i = r; r > 0; --i) {
B = ((B - S[2*i+1]) >>> A) ^ A;
A = ((A - S[2*i  ]) >>> B) ^ B;
}

return A, B;
}


// The RC5 Encryption Algorithm, Rivest '94
//
// key = 00.00 O0 O0 O0 O0 O0 O0 O0 O0 O0 O0 O0 O0 O0 O0
// plaintext 00000000 00000000
// ciphertext EEDBA521 6D8F4B15
//
// key = 91 5F 46 19 BE 41 B2 51 63 55 A5 01 10 A9 CE 91
// plaintext EEDBA521 6D8F4B15
// ciphertext ACI3COF7 52892B5B
//
// key = 78 33 48 E7 5A EB OF 2F D7 B1 69 BB 8D Ci 67 87
// plaintext ACI3COF7 52892B5B
// ciphertext B7B3422F 92FC6903
//
// key = DC 49 DB 13 75 A5 58 4F 64 85 B4 13 B5 FI 2B AF
// plaintext B7B3422F 92FC6903
// ciphertext B278C165 CC97D184
//
// key = 52 69 F1 49 D4 IB AO 15 24 97 57 4D 7F 15 31 25
// plaintext B278C165 CC97D184
// ciphertext 15E444EB 249831DA


// RC5_CBC
// R =  0
// Key = 00
// IV = 0000000000000000
// P = 0000000000000000
// C = 7a7bba4d79111d1e
//
// RC5_CBC
// R =  0
// Key = 00
// IV = 0000000000000000
// P = ffffffffffffffff
// C = 797bba4d78111d1e
//
// RC5_CBC
// R =  0
// Key = 00
// IV = 0000000000000001
// P = 0000000000000000
// C = 7a7bba4d79111d1f
//
// RC5_CBC
// R =  0
// Key = 00
// IV = 0000000000000000
// P = 0000000000000001
// C = 7a7bba4d79111d1f
//
// RC5_CBC
// R =  0
// Key = 00
// IV = 0102030405060708
// P = 1020304050607080
// C = 8b9ded91ce7794a6
//
// RC5_CBC
// R =  1
// Key = 11
// IV = 0000000000000000
// P = 0000000000000000
// C = 2f759fe7ad86a378
//
// RC5_CBC
// R =  2
// Key = 00
// IV = 0000000000000000
// P = 0000000000000000
// C = dca2694bf40e0788
//
// RC5_CBC
// R =  2
// Key = 00000000
// IV = 0000000000000000
// P = 0000000000000000
// C = dca2694bf40e0788
//
// RC5_CBC
// R =  8
// Key = 00
// IV = 0000000000000000
// P = 0000000000000000
// C = dcfe098577eca5ff
//
// RC5_CBC
// R =  8
// Key = 00
// IV = 0102030405060708
// P = 1020304050607080
// C = 9646fb77638f9ca8
//
// RC5_CBC
// R = 12
// Key = 00
// IV = 0102030405060708
// P = 1020304050607080
// C = b2b3209db6594da4
//
// RC5_CBC
// R = 16
// Key = 00
// IV = 0102030405060708
// P = 1020304050607080
// C = 545f7f32a5fc3836
//
// RC5_CBC
// R =  8
// Key = 01020304
// IV = 0000000000000000
// P = ffffffffffffffff
// C = 8285e7c1b5bc7402
//
// RC5_CBC
// R = 12
// Key = 01020304
// IV = 0000000000000000
// P = ffffffffffffffff
// C = fc586f92f7080934
//
// RC5_CBC
// R = 16
// Key = 01020304
// IV = 0000000000000000
// P = ffffffffffffffff
// C = cf270ef9717ff7c4
//
// RC5_CBC
// R = 12
// Key = 0102030405060708
// IV = 0000000000000000
// P = ffffffffffffffff
// C = e493f1c1bb4d6e8c
//
// RC5_CBC
// R =  8
// Key = 0102030405060708
// IV = 0102030405060708
// P = 1020304050607080
// C = 5c4c041e0f217ac3
//
// RC5_CBC
// R = 12
// Key = 0102030405060708
// IV = 0102030405060708
// P = 1020304050607080
// C = 921f12485373b4f7
//
// RC5_CBC
// R = 16
// Key = 0102030405060708
// IV = 0102030405060708
// P = 1020304050607080
// C = 5ba0ca6bbe7f5fad
//
// RC5_CBC
// R =  8
// Key = 01020304050607081020304050607080
// IV = 0102030405060708
// P = 1020304050607080
// C = c533771cd0110e63
//
// RC5_CBC
// R = 12
// Key = 01020304050607081020304050607080
// IV = 0102030405060708
// P = 1020304050607080
// C = 294ddb46b3278d60
//
// RC5_CBC
// R = 16
// Key = 01020304050607081020304050607080
// IV = 0102030405060708
// P = 1020304050607080
// C = dad6bda9dfe8f7e8
//
// RC5_CBC
// R = 12
// Key = 0102030405
// IV = 0000000000000000
// P = ffffffffffffffff
// C = 97e0787837ed317f
//
// RC5_CBC
// R =  8
// Key = 0102030405
// IV = 0000000000000000
// P = ffffffffffffffff
// C = 7875dbf6738c6478
//
// RC5_CBC
// R =  8
// Key = 0102030405
// IV = 7875dbf6738c6478
// P = 0808080808080808
// C = 8f34c3c681c99695
//
// RC5_CBC_Pad
// R =  8
// Key = 0102030405
// IV = 0000000000000000
// P = ffffffffffffffff
// C = 7875dbf6738c64788f34c3c681c99695
//
// RC5_CBC
// R =  8
// Key = 0102030405
// IV = 0000000000000000
// P = 0000000000000000
// C = 7cb3f1df34f94811
//
// RC5_CBC
// R =  8
// Key = 0102030405
// IV = 7cb3f1df34f94811
// P = 1122334455667701
// C = 7fd1a023a5bba217
//
// RC5_CBC_Pad
// R =  8
// Key = 0102030405
// IV = 0000000000000000
// P = ffffffffffffffff7875dbf6738c647811223344556677
// C = 7875dbf6738c64787cb3f1df34f948117fd1a023a5bba217
#endif
