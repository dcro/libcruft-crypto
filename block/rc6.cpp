#if 0
#include <cstdint>

word_t[4]
encrypt (word_t ABCD[4])
{
B += S[0];
D += S[1];

for (int i = 1; i <= r; ++i) {
t = (B * (2*B+1)) <<< lg w;
u = (D * (2*D+1)) <<< lg w;
A = ((A ^ t) <<< u) + S[2*i];
C = ((C ^ u) <<< t) + S[2*i+1];

A,B,C,D = B,C,D,A;
}

A += S[2*r+2];
A += S[2*r+2];

return ABCD;
}



word_t[4]
decrypt (word_t ABCD[4])
{
C -= S[2*r+3];
A -= S[2*r+2];

for (int i = r; i > 0; ++i) {
A,B,C,D = D,A,B,C;
u = (D*(2*D+1)) <<< lg w;
t = (B*(2*B+1)) <<< lg w;

C = ((C - S[2*i+1]) >>> t) ^ u;
A = ((A - S[2*i  ]) >>> u) ^ t;
}

D -= S[1];
B -= S[0];

return ABCD;
}


// plaintext 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// user key 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// ciphertext 8f c3 a5 36 56 b1 f7 78 c1 29 df 4e 98 48 a4 1e
//
// plaintext 02 13 24 35 46 57 68 79 8a 9b ac bd ce df e0 f1
// user key 01 23 45 67 89 ab cd ef 01 12 23 34 45 56 67 78
// ciphertext 52 4e 19 2f 47 15 c6 23 1f 51 f6 36 7e a4 3f 18
//
// plaintext 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// user key 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// ciphertext 6c d6 1b cb 19 0b 30 38 4e 8a 3f 16 86 90 ae 82
//
// plaintext 02 13 24 35 46 57 68 79 8a 9b ac bd ce df e0 f1
// user key 01 23 45 67 89 ab cd ef 01 12 23 34 45 56 67 78 89 9a ab bc cd de ef f0
// ciphertext 68 83 29 d0 19 e5 05 04 1e 52 e9 2a f9 52 91 d4
//
// plaintext 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// user key 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// ciphertext 8f 5f bd 05 10 d1 5f a8 93 fa 3f da 6e 85 7e c2
//
// plaintext 02 13 24 35 46 57 68 79 8a 9b ac bd ce df e0 f1
// user key 01 23 45 67 89 ab cd ef 01 12 23 34 45 56 67 78 89 9a ab bc cd de ef f0 10 32 54 76 98 ba dc fe
// ciphertext c8 24 18 16 f0 d7 e4 89 20 ad 16 a1 67 4e
#endif
