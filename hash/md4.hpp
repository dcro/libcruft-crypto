/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2013 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_MD4_HPP
#define __UTIL_HASH_MD4_HPP

#include <cruft/util/view.hpp>

#include <array>
#include <cstdint>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    class MD4 {
    public:
        using digest_t = std::array<uint8_t,16>;

        digest_t operator() (cruft::view<const uint8_t*>) noexcept;
    };
}

#endif
