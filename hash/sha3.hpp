/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2019, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

#include <array>


namespace cruft::crypto::hash {
    struct sha3 {
        using digest_t = std::array<u08,32>;

        digest_t operator() (cruft::view<u08 const *> data);
    };
};
