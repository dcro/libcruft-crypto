/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "md6.hpp"

using cruft::crypto::hash::MD6;


///////////////////////////////////////////////////////////////////////////////
#if 0
MD6::digest_t
MD6::operator() (cruft::view<const uint8_t*> data) const noexcept
{
    static constexpr int rounds = 40 + digest_size / 4;
    static constexpr int mode_control = 64;
    static constexpr int key_size = 0;

    static_assert (key_size >= 0 && key_size <= 64);
    static_assert (mode_control >= 0 && mode_control <= 64);

    digest_t d;
    return d;
};
#endif
