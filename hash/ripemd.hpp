/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2014-2018, Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_CRYPTO_HASH_RIPEMD_HPP
#define CRUFT_CRYPTO_HASH_RIPEMD_HPP

#include <cruft/util/view.hpp>

#include <array>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    class RIPEMD {
    public:
        typedef std::array<uint8_t,20> digest_t;

        digest_t operator() (cruft::view<const uint8_t*>);
    };
}

#endif
