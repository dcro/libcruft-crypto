/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/std.hpp>

#include <array>

namespace cruft::crypto::hash::detail {
    namespace blake {
        ///////////////////////////////////////////////////////////////////////
        template <int>
        struct traits {};


        //---------------------------------------------------------------------
        template <>
        struct traits<256>
        {
            using word_t = u32;

            static const std::array<word_t,8> iv;
            static const std::array<word_t,16> pi;
            static const std::array<int,4> rotations;
            static constexpr int rounds = 14;
        };


        //---------------------------------------------------------------------
        template <>
        struct traits<224>
        {
            using word_t = traits<256>::word_t;
        };


        //---------------------------------------------------------------------
        template <>
        struct traits<512>
        {
            using word_t = u64;

            static const std::array<word_t,8> iv;
            static const std::array<word_t,16> pi;
            static const std::array<int,4> rotations;
            static constexpr int rounds = 16;
        };


        //---------------------------------------------------------------------
        template <>
        struct traits<384>
        {
            using word_t = traits<512>::word_t;
        };
    }
};
