#include "traits.hpp"

using cruft::crypto::hash::detail::blake::traits;

///////////////////////////////////////////////////////////////////////////////
const std::array<traits<256>::word_t,8>
    traits<256>::iv
    {
        0x6A09E667, // frac(sqrt( 2))
        0xBB67AE85, // frac(sqrt( 3))
        0x3C6EF372, // frac(sqrt( 5))
        0xA54FF53A, // frac(sqrt( 7))
        0x510E527F, // frac(sqrt(11))
        0x9B05688C, // frac(sqrt(13))
        0x1F83D9AB, // frac(sqrt(17))
        0x5BE0CD19, // frac(sqrt(19))
    };


//-----------------------------------------------------------------------------
const std::array<traits<256>::word_t,16>
    traits<256>::pi
    {
        0x243F6A88,
        0x85A308D3,
        0x13198A2E,
        0x03707344,
        0xA4093822,
        0x299F31D0,
        0x082EFA98,
        0xEC4E6C89,
        0x452821E6,
        0x38D01377,
        0xBE5466CF,
        0x34E90C6C,
        0xC0AC29B7,
        0xC97C50DD,
        0x3F84D5B5,
        0xB5470917,
    };


//-----------------------------------------------------------------------------
const std::array<int,4>
    traits<256>::rotations {
    16, 12, 8, 7
};


///////////////////////////////////////////////////////////////////////////////
const std::array<traits<512>::word_t,8>
    traits<512>::iv {
    0x6A09E667F3BCC908,
    0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B,
    0xA54FF53A5F1D36F1,
    0x510E527FADE682D1,
    0x9B05688C2B3E6C1F,
    0x1F83D9ABFB41BD6B,
    0x5BE0CD19137E2179,
};


//-----------------------------------------------------------------------------
const std::array<traits<512>::word_t,16>
    traits<512>::pi {
    0x243F6A8885A308D3,
    0x13198A2E03707344,
    0xA4093822299F31D0,
    0x082EFA98EC4E6C89,
    0x452821E638D01377,
    0xBE5466CF34E90C6C,
    0xC0AC29B7C97C50DD,
    0x3F84D5B5B5470917,
    0x9216D5D98979FB1B,
    0xD1310BA698DFB5AC,
    0x2FFD72DBD01ADFB7,
    0xB8E1AFED6A267E96,
    0xBA7C9045F12C7F99,
    0x24A19947B3916CF7,
    0x0801F2E2858EFC16,
    0x636920D871574E69,
};


//-----------------------------------------------------------------------------
const std::array<int,4> traits<512>::rotations  { 32, 25, 16, 11 };

