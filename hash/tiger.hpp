/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

#include <array>

#include <cstddef>


namespace cruft::crypto::hash {
    struct tiger {
        using digest_t = std::array<u64,3>;

        digest_t operator() (cruft::view<const u08*>) noexcept;
    };
}