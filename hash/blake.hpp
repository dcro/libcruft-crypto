/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "blake/traits.hpp"

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

#include <array>

#include <cstdint>
#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    /// an implementation of the BLAKE hash function
    ///
    /// note that this is _not_ BLAKE2, but the original SHA-3 candidate
    /// function.
    ///
    /// \tparam width   the number of bits for the digest
    template <int width>
    class blake {
    public:
        using word_t = typename detail::blake::traits<width>::word_t;

        // size of each round's data block in bytes
        static const size_t block_size  = 16 * sizeof (word_t);

        // size of the digest in bytes
        static const size_t digest_size = width / 8;
        using digest_t = std::array<u08,digest_size>;


        digest_t operator() (
            cruft::view<const u08*> data,
            cruft::view<const u08*> salt
        ) const;

        digest_t operator() (cruft::view<const u08*> data,
                             const std::array<word_t,4> salt) const noexcept;

        digest_t operator() (cruft::view<const u08*> data) const noexcept
        {
            return (*this) (data, {0,0,0,0});
        }
    };
}
