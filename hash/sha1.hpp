/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2013-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_CRYPTO_HASH_SHA1_HPP
#define CRUFT_CRYPTO_HASH_SHA1_HPP

#include <cruft/util/view.hpp>

#include <array>
#include <cstdint>
#include <cstdlib>

///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    class SHA1 {
    public:
        using digest_t = std::array<uint8_t,20>;
        static const size_t BLOCK_SIZE  = 64;
        static const size_t DIGEST_SIZE = 20;


        //template <typename ...DataT>
        //digest_t
        //operator() (DataT&&...);

        digest_t operator() (cruft::view<const uint8_t*>, cruft::view<const uint8_t*>) noexcept;
        digest_t operator() (cruft::view<const uint8_t*>) noexcept;
    };
}

#endif
