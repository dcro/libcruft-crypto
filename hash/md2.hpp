/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2013-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/view.hpp>
#include <cruft/util/std.hpp>

#include <array>
#include <cstdint>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    class MD2 {
    public:
        typedef std::array<uint8_t,16> digest_t;

    public:
        digest_t operator() (cruft::view<u08 const*>) const noexcept;
    };
}
