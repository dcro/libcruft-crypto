/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2013-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_CRYPTO_HASH_MD5_HPP
#define CRUFT_CRYPTO_HASH_MD5_HPP

#include <cruft/util/view.hpp>

#include <array>
#include <cstdint>
#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::crypto::hash {
    class MD5 {
    public:
        using digest_t = std::array<uint8_t,16>;
        static const size_t BLOCK_SIZE  = 64;
        static const size_t DIGEST_SIZE = 16;

    public:
        digest_t operator() (cruft::view<const uint8_t*>) const noexcept;
        digest_t operator() (cruft::view<const uint8_t*>,cruft::view<const uint8_t*>) const noexcept;
        digest_t operator() (cruft::view<const uint8_t*>,cruft::view<const uint8_t*>,cruft::view<const uint8_t*>) const noexcept;

    private:
        std::array<uint32_t,4>
        transform (
            const std::array<uint32_t,4> &ABCD,
            const std::array<uint32_t,16> &X
        ) const noexcept;
    };
}

#endif
