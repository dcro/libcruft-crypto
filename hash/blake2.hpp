/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/std.hpp>
#include <cruft/util/view.hpp>

#include <array>


namespace cruft::crypto::hash {
    // RFC7693: The BLAKE2 Cryptographic Hash and Message Authentication Code
    class blake2 {
    public:
        using salt_t = std::array<uint8_t,64>;
        using digest_t = std::array<uint8_t,64>;

        blake2 () noexcept;
        blake2 (const salt_t&) noexcept;
        blake2 (cruft::view<const uint8_t*> key);

        digest_t operator() (cruft::view<const uint8_t*>) const noexcept;

    private:
        using state_t = std::array<uint64_t,8>;

        // we store zero padded salt because it simplifies later state
        // updates, not because it's a functional requirement. either way we
        // need to copy at least 64 bytes, so the user shouldn't be copying
        // these too much regardless.
        union {
            std::array<u08,128> val08;
            std::array<u64, 16> val64;
        } m_salt;

        uint64_t m_keylen;
    };
};
