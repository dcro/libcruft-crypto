/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_CRYPTO_HASH_HOTP_HPP
#define CRUFT_CRYPTO_HASH_HOTP_HPP

#include <cruft/util/view.hpp>

#include "hmac.hpp"
#include "sha1.hpp"

#include <cstdint>

namespace cruft::crypto::hash {
    /// HMAC one-time password (RFC 4226)
    class HOTP {
    public:
        HOTP (cruft::view<const char*> key, uint64_t counter);

        unsigned value (void);
        uint64_t counter (void) const;

    private:
        uint32_t truncate (SHA1::digest_t);

        uint64_t m_counter;
        HMAC<SHA1> m_hash;
    };
}

#endif
